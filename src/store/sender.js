import $http from '@/axios.settings.js'

export default {
  state: {
    cities: {}
  },
  getters: {
  },
  mutations: {
    SET_CITIES_LIST (state, cities) {
      state.cities = cities
    }
  },
  actions: {
    getCities: async ({ commit }) => {
      const { response } = await $http.get('/city')
      commit('SET_CITIES_LIST', response.data.data)

      return response.data.data
    }
  }
}
