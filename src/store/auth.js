import $http from '@/axios.settings.js'

export default {
  state: {
    token: localStorage.getItem('access_token') || null
  },
  getters: {
    isAuth ({ token }) {
      return Boolean(token)
    }
  },
  mutations: {
    SET_TOKEN (state, token) {
      state.token = `Bearer ${token}`
      localStorage.setItem('access_token', `Bearer ${token}`)
    }
  },
  actions: {
    login: async ({ commit }, payload) => {
      const { data } = await $http.post('/auth/login', payload)
      commit('SET_TOKEN', data.data.access_token)

      return data
    }
  }
}
