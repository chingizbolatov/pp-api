import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
import auth from './auth'
import sender from './sender'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    accessToken: localStorage.getItem('access_token') || '',
    currentUser: {}
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    sender
  },
  getters: {
    getAccessToken: state => {
      return state.accessToken
    }
  }
})
