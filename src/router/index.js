import Vue from 'vue'
import VueRouter from 'vue-router'

import Orders from '../views/Orders/index/index'
import CreateOrders from '../views/Orders/create/create'
import CreateSender from '../components/SenderForm/create'

import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'orders',
    component: Orders
  },
  {
    path: '/create',
    name: 'create-order',
    component: CreateOrders,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/sender/create',
    name: 'create-sender',
    component: CreateSender,
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const { isAuth } = store.getters

  if (to.meta.requiresAuth) {
    if (!isAuth) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
