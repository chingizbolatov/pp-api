import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VModal from 'vue-js-modal'
import App from './App.vue'
import router from './router'
import store from './store'
import $http from './axios.settings'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.prototype.$http = $http

Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(VModal)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
