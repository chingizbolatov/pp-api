import Axios from 'axios'
import router from '@/router'

const $http = Axios
$http.defaults.baseURL = 'http://logistics-crm.loc/api'
const accessToken = localStorage.getItem('access_token')

if (accessToken) {
  $http.defaults.headers.common.Authorization = accessToken
}

$http.interceptors.response.use(
  (response) => {
    if (response.status === 401 || response.status === 403) {
      localStorage.removeItem('access_token')
      router.push('/')
      document.location.reload()
    }
    return response
  },
  (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
      localStorage.removeItem('access_token')
      router.push('/')
      document.location.reload()
    }
    return Promise.reject(error)
  }
)

export default $http
